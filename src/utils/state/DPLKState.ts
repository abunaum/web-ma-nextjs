import {create} from 'zustand';

interface DPLKState {
    data: any[];
    pagedata: number[];
    loading: boolean;
    currentPage: number;
    keyword: string;
    setData: (data: any[]) => void;
    setPageData: (pagedata: number[]) => void;
    setLoading: (loading: boolean) => void;
    setCurrentPage: (currentpage: number) => void;
    setKeyword: (keyword: string) => void;
}

interface PageDetailState {
    urlgambar: null| string;
    data: any;
    urlshare: string;
    loading: boolean;
    setUrlGambar: (urlgambar: null|string) => void;
    setData: (data: any) => void;
    setUrlShare: (urlshare: string) => void;
    setLoading: (loading: boolean) => void;
}

export const DPLK = create<DPLKState>((set) => ({
    data: [],
    pagedata: [],
    loading: true,
    currentPage: 1,
    keyword: '',
    setData: (data) => set({ data }),
    setPageData: (pagedata) => set({ pagedata }),
    setLoading: (loading) => set({ loading }),
    setCurrentPage: (currentPage) => set({ currentPage }),
    setKeyword: (keyword) => set({ keyword }),
}));

export const PageDetailState = create<PageDetailState>((set) => ({
    urlgambar: null,
    data: null,
    urlshare: '',
    loading: true,
    setUrlGambar: (urlgambar) => set({ urlgambar }),
    setData: (data) => set({ data }),
    setUrlShare: (urlshare) => set({ urlshare }),
    setLoading: (loading) => set({ loading }),
}));