export const UCF = (str: string) => {
    const lcstr = str.toLowerCase();
    return lcstr.charAt(0).toUpperCase() + lcstr.slice(1);
}