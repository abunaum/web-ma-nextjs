export type CommentTypes = {
    id?: string;
    komentar?: string;
    user?: string;
    userEmail?: string;
    userImage?: string;
    created_at?: any;

}