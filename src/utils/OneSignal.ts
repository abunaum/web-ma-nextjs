import OneSignal from 'react-onesignal';

export default async function runOneSignal() {
    const appId = process.env.NEXT_PUBLIC_ONESIGNAL_APP_ID ?? '';
    const safari_web_id = process.env.NEXT_PUBLIC_ONESIGNAL_SAFARI_WEB_ID ?? '';
    await OneSignal.init({
        appId,
        safari_web_id,
        autoResubscribe: true,
        autoRegister: true,
        notifyButton: {
            enable: true,
        },
        allowLocalhostAsSecureOrigin: true
    });
    await OneSignal.Slidedown.promptPush();
}