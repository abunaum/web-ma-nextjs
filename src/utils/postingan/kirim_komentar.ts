import axios from "axios";

const buatKomentar = async (text:string, session: any) => {
    if (text === ''){
        return {
            success: false,
            message: "Komentar tidak boleh kosong",
            data: null
        }
    }
    if (text.length > 300){
        return {
            success: false,
            message: "Komentar terlalu panjang, Maksimal 300 karakter",
            data: null
        }
    }
    const newComment = {
        komentar: text,
        user: session.user.name,
        userEmail: session.user.email,
        userImage: session.user.image,
    }
    return {
        success: true,
        message: "Komentar berhasil dibuat",
        data: newComment
    };
};

const kirimKomentar = async (id:string, data: any) => {
    const api = process.env.NEXT_PUBLIC_API_URL ?? "";
    try {
        const kirim = await axios.post(`${api}/api/post/komentar/tambah/${id}`, data);
        if (!kirim.data.success) {
            return {
                success: false,
                message: "Komentar gagal dikirim",
            }
        }
        return {
            success: true,
            message: "Komentar berhasil dikirim",
            data: kirim.data.data
        }
    } catch (error) {
        return {
            success: false,
            message: "Komentar gagal dikirim",
        }
    }
}

export {buatKomentar, kirimKomentar};