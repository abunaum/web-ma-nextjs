import React from 'react';
import styles from "@/styles/pimpinan_card.module.css";
import Image from "next/image";

const Index = () => {
    return (
        <React.Fragment>
            <section id="team" className="team section-bg">
                <div className="container" data-aos="fade-up">
                    <div className="breadcrumbs" data-aos="fade-in">
                        <div className="section-title">
                            <h2>Pimpinan Madrasah</h2>
                        </div>
                        <div className="container mt-3">
                            <div className={styles.flexbox}>
                                <div className={`${styles.flexcard} ${styles.flexcardGreen}`}>
                                    <div className={`${styles.flexcardNumber} ${styles.flexcardNumberGreen}`}>01</div>
                                    <div className={`${styles.flex} ${styles.flexcardTitle}`}>Title</div>
                                    <div className={`${styles.flex} ${styles.flexcardText}`}>Lorem ipsum dolor sit, amet
                                        consectetur adipisicing elit. Recusandae,
                                        temporibus consectetur? Iure id nam fuga asperiores repellat accusantium
                                        exercitationem nemo?
                                    </div>
                                    <div className={`${styles.flex} ${styles.flexcardimgItem}`}>
                                        <Image
                                            src="/assets/img/search.png"
                                            alt="Logo Animasi"
                                            className={styles.flexcardimgItem}
                                            width={300}
                                            height={300}
                                            priority
                                            style={{width: "5em", height: "5em"}}
                                        />
                                    </div>
                                </div>
                                <div className={`${styles.flexcard} ${styles.flexcardBlue}`}>
                                    <div className={`${styles.flexcardNumber} ${styles.flexcardNumberBlue}`}>02</div>
                                    <div className={`${styles.flex} ${styles.flexcardTitle}`}>Title</div>
                                    <div className={`${styles.flex} ${styles.flexcardText}`}>Lorem ipsum dolor sit, amet
                                        consectetur adipisicing elit. Recusandae,
                                        temporibus consectetur? Iure id nam fuga asperiores repellat accusantium
                                        exercitationem nemo?
                                    </div>
                                    <div className={`${styles.flex} ${styles.flexcardimgItem}`}>
                                        <Image
                                            src="/assets/img/search.png"
                                            alt="Logo Animasi"
                                            className={styles.flexcardimgItem}
                                            width={300}
                                            height={300}
                                            priority
                                            style={{width: "5em", height: "5em"}}
                                        />
                                    </div>
                                </div>
                                <div className={`${styles.flexcard} ${styles.flexcardOrange}`}>
                                    <div className={`${styles.flexcardNumber} ${styles.flexcardNumberOrange}`}>03</div>
                                    <div className={`${styles.flex} ${styles.flexcardTitle}`}>Title</div>
                                    <div className={`${styles.flex} ${styles.flexcardText}`}>Lorem ipsum dolor sit, amet
                                        consectetur adipisicing elit. Recusandae,
                                        temporibus consectetur? Iure id nam fuga asperiores repellat accusantium
                                        exercitationem nemo?
                                    </div>
                                    <div className={`${styles.flex} ${styles.flexcardimgItem}`}>
                                        <Image
                                            src="/assets/img/search.png"
                                            alt="Logo Animasi"
                                            className={styles.flexcardimgItem}
                                            width={300}
                                            height={300}
                                            priority
                                            style={{width: "5em", height: "5em"}}
                                        />
                                    </div>
                                </div>
                                <div className={`${styles.flexcard} ${styles.flexcardPink}`}>
                                    <div className={`${styles.flexcardNumber} ${styles.flexcardNumberPink}`}>04</div>
                                    <div className={`${styles.flex} ${styles.flexcardTitle}`}>Title</div>
                                    <div className={`${styles.flex} ${styles.flexcardText}`}>Lorem ipsum dolor sit, amet
                                        consectetur adipisicing elit. Recusandae,
                                        temporibus consectetur? Iure id nam fuga asperiores repellat accusantium
                                        exercitationem nemo?
                                    </div>
                                    <div className={`${styles.flex} ${styles.flexcardimgItem}`}>
                                        <Image
                                            src="/assets/img/search.png"
                                            alt="Logo Animasi"
                                            className={styles.flexcardimgItem}
                                            width={300}
                                            height={300}
                                            priority
                                            style={{width: "5em", height: "5em"}}
                                        />
                                    </div>
                                </div>
                                {/*<div className="flexcard flexcardOrange">*/}
                                {/*    <div className="flexcardNumber flexcardNumberOrange">03</div>*/}
                                {/*    <div className="flex flexcardTitle">Title</div>*/}
                                {/*    <div className="flex flexcardText">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Recusandae,*/}
                                {/*        temporibus consectetur? Iure id nam fuga asperiores repellat accusantium exercitationem nemo?</div>*/}
                                {/*    <div className="flex flexcardImg"><img className="flexcardimgItem"*/}
                                {/*                                       src="https://cdn.pixabay.com/photo/2017/01/10/23/01/seo-1970475_960_720.png" alt=""></div>*/}
                                {/*</div>*/}
                                {/*<div class="flexcard flexcardPink">*/}
                                {/*    <div class="flexcardNumber flexcardNumberPink">04</div>*/}
                                {/*    <div class="flex flexcardTitle">Title</div>*/}
                                {/*    <div class="flex flexcardText">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Recusandae,*/}
                                {/*        temporibus consectetur? Iure id nam fuga asperiores repellat accusantium exercitationem nemo?</div>*/}
                                {/*    <div class="flex flexcardImg"><img class="flexcardimgItem"*/}
                                {/*                                       src="https://cdn.pixabay.com/photo/2017/01/10/23/01/seo-1970475_960_720.png" alt=""></div>*/}
                                {/*</div>*/}
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </React.Fragment>
    );
};

export default Index;