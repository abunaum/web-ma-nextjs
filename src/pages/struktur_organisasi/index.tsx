import Image from "next/image";

export default function Struktur_Organisasi() {
    return (
        <>
            <section id="team" className="team section-bg">
                <div className="container" data-aos="fade-up">
                    <div className="breadcrumbs" data-aos="fade-in">
                        <div className="section-title">
                            <h2>Struktur organisasi</h2>
                        </div>
                        <div className="container mt-3">
                            <div className="mb-3 justify-content-center text-center">
                                <Image
                                    src="/assets/img/struktur.png"
                                    alt="Logo Animasi"
                                    className="img-fluid"
                                    width={900}
                                    height={1500}
                                    priority
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}
