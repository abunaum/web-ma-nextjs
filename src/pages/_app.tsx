import type {AppProps} from 'next/app';
import TopBar from "@/components/layout/topbar";
import FOOTER from "@/components/layout/footer";
import MaintenancePage from '@/components/MaintenancePage';
import "@/styles/globals.css";
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-icons/font/bootstrap-icons.css';
import React, {useEffect} from 'react';
import runOneSignal from "@/utils/OneSignal";
import {SessionProvider} from "next-auth/react"

export default function App({Component, pageProps: {session, ...pageProps}}: AppProps) {
    useEffect(() => {
        runOneSignal();
    }, []);
    const isMaintenance = false;
    if (isMaintenance) {
        return <MaintenancePage />;
    }
    return (
        <>
            <SessionProvider session={session}>
                <TopBar/>
                <Component {...pageProps} />
                <FOOTER/>
            </SessionProvider>
        </>
    )
}
