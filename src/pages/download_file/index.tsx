import React from 'react';
import Swal from "sweetalert2";
import Head from "next/head";

const Index = () => {
    return (
        <>
            <Head>
                <title>Download File</title>
                <meta name="description" content="Halaman download file resmi MA Zainul Hasan 1 Genggong"/>
                <meta property="og:image"
                      content={`/assets/img/LOGO.png`}/>
                <meta property="og:title" content="Download File"/>
            </Head>
            <div className="breadcrumbs" data-aos="fade-in">
                <div className="container mt-5">
                    <div className="section-title">
                        <h2>Pusat Download Informasi Ma zainul Hasan 1 Genggong</h2>
                    </div>
                </div>
            </div>
            <section>
                <div className="container" data-aos="fade-up">
                    <div className="row justify-content-center">
                        <div className="card m-3" style={{"cursor": "pointer"}} onClick={
                            () => {
                                    window.open('https://mazainulhasan1.sch.id/assets/file/PENGUMUMAN_HASIL_TES_SANTRI_BARU_MA_ZAHA_2024.pdf', '_blank', 'noopener')
                            }}>
                            <div className="card-body">
                                <h3>Pengumuman Hasil Tes CBT Dan Baca Kitab Calon Santri Baru<br/>Tahun Pelajaran 2024-2025</h3>
                            </div>
                        </div>
                        <div className="card m-3" style={{"cursor": "pointer"}} onClick={
                            () => {
                                const serverTime = new Date();
                                const targetDate = new Date(2024, 4, 6, 12, 0, 0);

                                if (serverTime < targetDate) {
                                    maintenance()
                                } else {
                                    window.open('https://mazainulhasan1.sch.id/assets/file/SK_KELULUSAN_2024.pdf', '_blank', 'noopener')
                                }
                            }}>
                            <div className="card-body">
                                <h3>Pengumuman Kelulusan Santri Kelas 12 Tahun Pelajaran 2023-2024</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
};

function maintenance() {
    Swal.fire({
        title: 'Maaf',
        text: 'Belum Waktunya Mendownload File Ini',
        icon: 'info',
        showConfirmButton: false,
        timer: 1500
    })
}

export default Index;