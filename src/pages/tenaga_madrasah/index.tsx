import React, { useState } from "react";
import PersonCategory from "@/components/person_madrasah";

interface CategoryInfo {
    nama: string;
    url: string;
}

interface Categories {
    [key: string]: CategoryInfo;
}

export default function Card_gs() {
    const categories: Categories = {
        pimpinan: {
            nama: "Pimpinan Madrasah",
            url: "api/gs/pimpinan"
        },
        walikelas: {
            nama: "Wali Kelas",
            url: "api/gs/wali-kelas"
        },
        guru: {
            nama: "Tenaga Pendidik",
            url: "api/gs/guru"
        },
        karyawan: {
            nama: "Staff dan Karyawan",
            url: "api/gs/karyawan"
        }
    };

    const [activeCategory, setActiveCategory] = useState<string>("pimpinan");

    const handleCategoryChange = (newCategory: string) => {
        setActiveCategory(newCategory);
    };

    return (
        <>
            <section className="mt-5" style={{ marginBottom: "-40px" }}>
                <center>
                    <div className="btn-group">
                        {Object.keys(categories).map((categoryKey) => (
                            <button
                                key={categoryKey}
                                type="button"
                                className={`btn ${
                                    activeCategory === categoryKey ? "btn-primary" : "btn-outline-primary"
                                }`}
                                onClick={() => handleCategoryChange(categoryKey)}
                            >
                                {categories[categoryKey].nama}
                            </button>
                        ))}
                    </div>
                </center>
            </section>
            {Object.keys(categories).map((categoryKey) => (
                activeCategory === categoryKey && <PersonCategory key={categoryKey} tipe={categories[categoryKey]} />
            ))}
        </>
    );
}