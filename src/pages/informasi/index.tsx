import Image from "next/image";
import React from "react";
import Link from "next/link";
import Swal from "sweetalert2";

export default function Informasi() {
    return (
        <>
            <div className="breadcrumbs" data-aos="fade-in">
                <div className="container mt-3">
                    <div className="section-title">
                        <h2>Pusat Informasi Ma zainul Hasan 1 Genggong</h2>
                    </div>
                </div>
            </div>
            <section>
                <div className="container" data-aos="fade-up">
                    <div className="row justify-content-center">
                        <div className="col-xs-12 col-sm-4" style={{padding: 25}}>
                            <div className="card">
                                <div className="img-card">
                                    <Image
                                        src="/assets/img/news.png"
                                        alt="Card image cap"
                                        className="img-thumbnail"
                                        width={800}
                                        height={800}
                                        priority
                                        style={{
                                            height: "20em",
                                            width: "20em"
                                        }}
                                    />
                                </div>
                                <div className="card-content">
                                    <h4 className="card-title mt-3">
                                        Berita Seputar Madrasah
                                    </h4>
                                    <p className="">
                                        Pusat informasi dan berita seputar madrasah ma zainul hasan 1 genggong.
                                    </p>
                                </div>
                                <div className="card-read-more">
                                    <Link
                                        href={{
                                            pathname: '/berita',
                                        }}
                                        className="btn btn-info mb-2"
                                        style={{cursor: 'pointer'}}
                                    >
                                        Lihat Selengkapnya
                                    </Link>
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-4" style={{padding: 25}}>
                            <div className="card">
                                <div className="img-card">
                                    <Image
                                        src="/assets/img/fiqih.png"
                                        alt="Card image cap"
                                        className="img-thumbnail"
                                        width={800}
                                        height={800}
                                        priority
                                        style={{
                                            height: "20em",
                                            width: "20em"
                                        }}
                                    />
                                </div>
                                <div className="card-content">
                                    <h4 className="card-title mt-3">
                                        Tanya Jawab Fiqih
                                    </h4>
                                    <p className="">
                                        Pusat informasi dan berita seputar kajian fiqih dan tanya jawab.
                                    </p>
                                </div>
                                <div className="card-read-more">
                                    <Link
                                        href={{
                                            pathname: '/tanya_jawab',
                                        }}
                                        className="btn btn-info mb-2"
                                        style={{cursor: 'pointer'}}
                                    >
                                        Lihat Selengkapnya
                                    </Link>
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-4" style={{padding: 25}}>
                            <div className="card">
                                <div className="img-card">
                                    <Image
                                        src="/assets/img/calendar.png"
                                        alt="Card image cap"
                                        className="img-thumbnail"
                                        width={800}
                                        height={800}
                                        priority
                                        style={{
                                            height: "20em",
                                            width: "20em"
                                        }}
                                    />
                                </div>
                                <div className="card-content">
                                    <h4 className="card-title mt-3">
                                        Agenda Kegiatan Madrasah
                                    </h4>
                                    <p className="">
                                        Pusat informasi seputar agenda kegiatan madrasah.
                                    </p>
                                </div>
                                <div className="card-read-more">

                                    <Link
                                        href="#"
                                        className="btn btn-info mb-2"
                                        style={{cursor: 'pointer'}}
                                        onClick={maintenance}
                                    >
                                        Lihat Selengkapnya
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

function maintenance() {
    Swal.fire({
        title: 'Maaf',
        text: 'Fitur ini sedang dalam pengembangan',
        icon: 'info',
        showConfirmButton: false,
        timer: 1500
    })
}
