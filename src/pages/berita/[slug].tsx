import React from 'react';
import axios from 'axios';
import { useRouter } from 'next/router';
import Head from 'next/head';
import Spinner from "@/components/spinner";
import { useSession, signIn } from "next-auth/react";
import KomentarCard from "@/components/card/komentar_card";
import BuatKomentarCard from "@/components/card/buat_komentar_card";
import useSWR from "swr";
import BeritaDetailCard from "@/components/card/berita_detail_card";

interface BeritaDetailPageProps {
    berita: any;
}

const fetcher = (url: string) => axios.get(url).then((res) => res.data);

const BeritaDetailPage: React.FC<BeritaDetailPageProps> = ({ berita }) => {
    const router = useRouter();
    const { data: session } = useSession();
    const { data, isLoading, error, mutate } = useSWR(
        berita ? `${process.env.NEXT_PUBLIC_API_URL}/api/post/slug/${berita.slug}` : null,
        fetcher,
        { initialData: berita, revalidateOnMount: true }
    );

    if (isLoading) {
        return <Spinner text={""} />;
    }

    if (error) {
        return <Spinner text={"Gagal terkoneksi ke server, silahkan refresh halaman"} />;
    }

    return (
        <>
            <Head>
                <title>{data.judul}</title>
                <meta name="description" content={data.excerpt}/>
                <meta property="og:image"
                      content={`https://res.cloudinary.com/dfko3mdsp/image/upload/v1690253988/${data.gambar}.png`}/>
                <meta property="og:title" content={data.judul}/>
                {/* Additional meta tags */}
            </Head>
            <div className="breadcrumbs" data-aos="fade-in">
            <div className="container mt-3">
                    <div className="row justify-content-center">
                        <div className="col-md-6">
                        </div>
                    </div>
                </div>
            </div>
            <section>
                <div className="container" data-aos="fade-up">
                    <BeritaDetailCard
                        data={data}
                        urlshare={`${window.location.origin}/berita/${router.query.slug}`}
                        urlgambar={`https://res.cloudinary.com/dfko3mdsp/image/upload/c_limit,w_1200/f_auto/q_auto/v1/${data.gambar}`}
                    />
                    {session ? (
                        <BuatKomentarCard postid={data.id} mutate={mutate} />
                    ) : (
                        <div className="card">
                            <button className="btn btn-success" onClick={() => signIn()}>Masuk untuk berkomentar</button>
                        </div>
                    )}
                    <hr />
                    {data.comment && (
                        <KomentarCard data={data.comment} postid={data.id} mutate={mutate} session={session} />
                    )}
                </div>
            </section>
        </>
    );
}

export async function getServerSideProps(context: any) {
    const slug = context.query.slug;
    const api = process.env.NEXT_PUBLIC_API_URL;
    const res = await axios.get(`${api}/api/post/slug/${slug}`);
    const berita = res.data;

    return {
        props: {
            berita,
        },
    };
}

export default BeritaDetailPage;