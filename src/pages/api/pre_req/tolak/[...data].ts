import type {NextApiRequest, NextApiResponse} from 'next'
import axios from "axios";

type Data = {
    success?: boolean
    name?: string
    id?: string
    secret?: string
    error?: string
    cek?: any
    data?: any
    message?: string
}

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<Data>
) {
    const [id, secret] = req.query.data as string[];
    if (!id || !secret) {
        res.status(400).json({error: 'Bad request'})
        return;
    }
    try {
        const del = await axios.delete(`${process.env.NEXT_PUBLIC_API_URL}/api/kirim_pertanyaan/${id}/${secret}`)
        if (!del.data.success) {
            return res.status(200).json({error: del.data})
        }
        res.status(200).json({success: true, message: 'Pertanyaan berhasil dihapus'})
    } catch (e: any) {
        let msg
        if (e.response) {
            msg = e.response.data.message
        } else {
            msg = e.message
        }
        return res.status(200).json({error: e})
    }
}
