import type {NextApiRequest, NextApiResponse} from 'next'
import axios from "axios";

type Data = {
    success?: boolean
    name?: string
    id?: string
    secret?: string
    error?: string
    cek?: any
    data?: any
}

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<Data>
) {
    const [id, secret] = req.query.data as string[];
    if (!id || !secret) {
        res.status(400).json({error: 'Bad request'})
        return;
    }
    try {
        const cek = await axios.post(`${process.env.NEXT_PUBLIC_API_URL}/api/kirim_pertanyaan/terima`, {
            id,
            secret
        });
        if (cek.data.success) {
            res.status(200).json({success: true, data: cek.data})
        } else {
            return res.status(200).json({error: cek.data})
        }

    } catch (e: any) {
        let msg
        if (e.response) {
            msg = e.response.data.message
        } else {
            msg = e.message
        }
        return res.status(200).json({error: e})
    }
}
