import React from "react";
import ProgramUnggulanCard from "@/components/card/program_unggulan_card";
import Intra_Card from "@/components/card/intra_card";
import Extra_Card from "@/components/card/extra_card";

export default function Program() {
    const pu = [
        {
            nama: "Tahfidzul Qur'an",
            image: "/assets/img/program/tahf.jpg",
            text: "Program untuk santri yang berminat untuk menghafal al-qur'an. Program ini bekerjasama dengan Jam'iyyatul Qurro' wal Huffadz cabang kota Kraksaan."
        },
        {
            nama: "Prodistik",
            image: "/assets/img/program/prod.jpg",
            text: "Program Studi Intensifikasi TIK (Prodistik) resmi menggandeng Departemen Teknologi Pendidikan, Fakultas Ilmu Pendidikan, Universitas Negeri Malang (UM) sebagai mitra dalam pengembangan multimedia, broadcasting, fotografi, sinematografi, AI dan pembuatan aplikasi media pembelajaran."
        },
        {
            nama: "Tahqiqu Qiroatil Kutub",
            image: "/assets/img/program/tahq.jpg",
            text: "Program Tahqiqu Qiroatil Kutub adalah kelas unggulan untuk santri yang ingin mendalami khazanah ilmu keislaman, kajian kitab kuning, dan bahasa Arab. Program ini bekerja sama dengan Fakultas Humaniora Jurusan Bahasa dan Sastra Arab UIN Maulana Malik Ibrahim Malang."
        }
    ];
    const intra = [
        {
            nama: "MIPA",
            image: "/assets/img/program/ipa.png",
        },
        {
            nama: "ISS",
            image: "/assets/img/program/ips.png",
        },
        {
            nama: "PK",
            image: "/assets/img/program/kitab.png",
        }
    ];
    const extra = [
        {
            image: '/assets/img/program/extra/pramuka.jpg',
            title: 'Pramuka',
        },
        {
            image: '/assets/img/program/extra/reactjs.jpg',
            title: 'KIR',
        },
        {

            image: '/assets/img/program/extra/al-quran.png',
            title: 'Tartilul Qur\'an',
        },
        {

            image: '/assets/img/program/extra/silat.jpg',
            title: 'Pagar Nusa',
        },
        {

            image: '/assets/img/program/extra/english_club.jpg',
            title: 'English Club',
        },
        {

            image: '/assets/img/program/extra/arabic.jpg',
            title: 'Arabic Club',
        },
        {
            image: '/assets/img/program/extra/olimpiade.jpg',
            title: 'Kelas Olimpiade',
        }

    ];
    return (
        <>
            <div className="breadcrumbs" data-aos="fade-in">
                <div className="container mt-3">
                    <div className="section-title">
                        <h2>Program Unggulan</h2>
                    </div>
                </div>
            </div>
            <section>
                <div className="container" data-aos="fade-up">
                    <div className="row justify-content-center">
                        <ProgramUnggulanCard data={pu}/>
                    </div>
                </div>
            </section>

            <div className="breadcrumbs" data-aos="fade-in">
                <div className="container mt-3">
                    <div className="section-title">
                        <h2>INTRAKULIKULER</h2>
                    </div>
                </div>
            </div>
            <section>
                <div className="container" data-aos="fade-up">
                    <div className="row justify-content-center">
                        <Intra_Card data={intra}/>
                    </div>
                </div>
            </section>

            <div className="breadcrumbs" data-aos="fade-in">
                <div className="container mt-3">
                    <div className="section-title">
                        <h2>EKSTRAKULIKULER</h2>
                    </div>
                </div>
            </div>
            <section>
                <div className="container" data-aos="fade-up">
                    <div className="row justify-content-center">
                        <Extra_Card data={extra} />
                    </div>
                </div>
            </section>
            <hr/>
        </>
    )
}
