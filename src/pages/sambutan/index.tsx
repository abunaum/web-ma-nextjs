export default function Sambutan() {
    return (
        <>
            <section id="team" className="team section-bg">
                <div className="container" data-aos="fade-up">
                    <div className="breadcrumbs" data-aos="fade-in">
                        <div className="section-title">
                            <h2>Sambutan Kepala Madrasah</h2>
                        </div>
                        <div className="container mt-3">
                            <div>
                                <div style={{textAlign: "center"}}>
                                    <h3 className="ql-align-justify">
                                        Assalamu’alaikum Warohmatullahi Wabarokatuh…
                                    </h3>
                                    <p className="ql-align-justify">
                                        Hamidan lillah, tabaaroka wata’ala Wa Musholliyan ala Rosulillah.
                                    </p>
                                </div>
                                <p className="ql-align-justify">
                                    Atas nama pribadi mewakili segenap Pimpinan Madrasah Aliyah serta Dewan Asatid
                                    mengucapkan
                                    selamat atas hadirnya Website Madrasah Aliyah Zainul Hasan 1 Genggong yang
                                    launchingnya pada
                                    hari kamis tanggal 22 Mei 2014, bertepatan dengan acara Pisah kenang untuk kelas XII
                                    Putri.
                                </p>
                                <p className="ql-align-justify">
                                    Semoga, dengan hadirnya Website MA. ZaHA 1 Genggong, kita bisa ikut berpartisipasi
                                    dalam
                                    pengembangan informasi melalui media IT yang sudah berkembang pesat di era ini.
                                    Harapan
                                    kami, tim website yang sudah terbentuk dapat memberikan warna baru bagi kemaslahatan
                                    Madrasah serta mengupdate informasi terbaru bagi pembaca terutama dalam cakupan
                                    dunia maya.
                                    Website MA. Zainul Hasan 1 Genggong hadir untuk menunjukkan existensi kreatifitas
                                    lembaga
                                    dalam menunjang kemampuan ber-IT untuk menampilkan berbagai kegiatan yang ada di
                                    Madrasah,
                                    intra maupun extra, terutama kegiatan-kegiatan unggulan kami agar bisa diterima
                                    dengan cepat
                                    di masyarakat. Website ini pula, kami niatkan sebagai media dakwah bil qolam yang
                                    contentnya
                                    akan kami muat dari hasil pemikiran dan ide para santri-santri yang berada di bawah
                                    naungan
                                    lembaga aliyah. Sekali lagi, kami mengucapkan selamat atas hadirnya Website MA.
                                    Zainul Hasan
                                    1 Genggong. Semoga bermanfaat.
                                </p>
                                <div style={{textAlign: "center"}}>
                                    <h3 className="ql-align-justify">
                                        Wassalamu’alaikum, wr.wb.
                                    </h3>
                                    <p className="ql-align-justify">
                                        Genggong, 22 Mei 2014
                                    </p>
                                    <p className="ql-align-justify">
                                        Hormat kami:
                                    </p>
                                    <p className="ql-align-justify">
                                        Kepala Madrasah
                                        <br/>
                                        <br/>
                                        <br/>
                                    </p>
                                    <p className="ql-align-justify">
                                        <strong>
                                            <u>
                                                Nun AHSAN MALIKI, S.Sy. M.Pd
                                            </u>
                                        </strong>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}
