import Hero from "@/components/homepage/hero";
import Pilih_kami from "@/components/homepage/pilih_kami";
import Text_Arab from "@/components/homepage/text_arab";
import Fasilitas from "@/components/homepage/fasilitas";
import FAQ from "@/components/homepage/faq";
import CTA from "@/components/homepage/cta";

export default function Home() {
    return (
        <>
            <Hero/>
            <main id="main">
                <Text_Arab/>
                <Pilih_kami/>
                <Fasilitas/>
                <FAQ/>
                <CTA/>
            </main>
        </>
    )
}
