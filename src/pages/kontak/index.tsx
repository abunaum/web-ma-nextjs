import styles from "@/styles/pages/kontak.module.css";

export default function Kontak() {
    return (
        <>
            <div className="breadcrumbs" data-aos="fade-in">
                <div className="container mt-3">
                    <div className="section-title">
                        <h2>Kontak</h2>
                    </div>
                </div>
            </div>
            <section className={styles.contact}>
                <div className="container" data-aos="fade-up">
                    <div className="row">
                        <div className="col-lg-5 d-flex align-items-stretch">
                            <div className={styles.info}>
                                <div className={styles.address}>
                                    <i className="bi bi-geo-alt"></i>
                                    <h4>Lokasi:</h4>
                                    <p>Jl. Raya Condong No.12, Gerojokan
                                        Karangbong, Kec. Pajarakan
                                        Kab. Probolinggo, Jawa Timur, 67281</p>
                                </div>

                                <div className={styles.email}>
                                    <i className="bi bi-envelope"></i>
                                    <h4>Email:</h4>
                                    <p>mazainulhasan1@gmail.com</p>
                                </div>

                                <div className={styles.phone}>
                                    <i className="bi bi-phone"></i>
                                    <h4>Telepon:</h4>
                                    <p>(0335) 842253</p>
                                </div>

                                <iframe
                                    src="https://maps.google.com/maps?q=ma%20zainul%20hasan%201%20genggong&t=&z=13&ie=UTF8&iwloc=&output=embed"
                                    frameBorder="0" style={{border: 0, width: "100%", height: "290px"}}
                                    allowFullScreen></iframe>
                            </div>

                        </div>

                        <div className="col-lg-7 mt-5 mt-lg-0 d-flex align-items-stretch">
                            <form action="#" className="php-email-form">
                                <div className="row">
                                    <div className="form-group col-md-6">
                                        <label htmlFor="name">Nama</label>
                                        <input type="text" name="name" className="form-control" id="name" required/>
                                    </div>
                                    <div className="form-group col-md-6">
                                        <label htmlFor="name">Email</label>
                                        <input type="email" className="form-control" name="email" id="email" required/>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="name">Subject</label>
                                    <input type="text" className="form-control" name="subject" id="subject" required/>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="name">Pesan</label>
                                    <textarea className="form-control" name="message" rows={10} required></textarea>
                                </div>
                                <div className="my-3">
                                    <div className="loading">Loading</div>
                                    <div className="error-message"></div>
                                    <div className="sent-message">Pesan anda telah terkirim, Terima Kasih!</div>
                                </div>
                                <div className="text-center">
                                    <button type="button" className={"btn btn-primary"}>Kirim Pesan</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </section>
        </>
    )
}
