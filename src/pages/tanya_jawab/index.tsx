import React from 'react';
import {DPLK} from "@/utils/state/DPLKState";
import axios from "axios";
import Spinner from "@/components/spinner";
import Pagination from "@/components/pagination/pagination";
import TanyaJawab from "@/components/card/tanya_jawab";
import {Button, Modal} from "react-bootstrap";
import {LoadingTimer, showWaitLoading} from "@/components/waitLoading";
import {useForm} from "react-hook-form";
import {IFormValues, Input, TextArea} from "@/components/form/kirim_pertanyaan";

const Index = () => {
    const {
        data,
        setData,
        pagedata,
        setPageData,
        loading,
        setLoading,
        currentPage,
        setCurrentPage,
        keyword,
        setKeyword,
    } = DPLK();

    const [showModal, setShowModal] = React.useState<boolean>(false);
    const {register: fdata, handleSubmit} = useForm<IFormValues>();
    React.useEffect(() => {
        fetchData(1, keyword).then(() => {
            console.log("Data telah diambil");
        });
    }, [keyword]);
    const fetchData = async (page: number, searchKeyword: string) => {
        setLoading(true);
        const result = await getData(page, searchKeyword);
        setData(result.data);
        setPageData([
            result.length ?? 0,
            result.recordsTotal ?? 0,
            result.recordsFiltered ?? 0,
            result.start ?? 0,
        ]);
        setLoading(false);
    };

    const handleSearch = (searchKeyword: string) => {
        setKeyword(searchKeyword);
    };

    const handlePageChange = (page: number) => {
        setCurrentPage(page);
        fetchData(page, keyword).then(() => {
            console.log("Data telah diambil");
        });
    };
    const handleKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (event.key === 'Enter') {
            event.preventDefault();
            const searchKeyword: string = (document.getElementById("search-input") as HTMLInputElement).value;
            handleSearch(searchKeyword);
        }
    };
    const onSubmit = async (data: any) => {
        const id = (new Date().getTime()).toString(6)
        const key = localStorage.getItem("id_QAF")
        let fix_id
        if (!key) {
            localStorage.setItem("id_QAF", id)
            fix_id = id
        } else {
            fix_id = key
        }
        try {
            let nama
            if (data.nama === "") {
                nama = "Hamba Allah"
            } else {
                nama = data.nama
            }
            showWaitLoading("Mengirim pertanyaan");
            const api = process.env["NEXT_PUBLIC_API_URL"];
            const request = await axios.post(`${api}/api/kirim_pertanyaan/kirim`, {
                id_penanya: fix_id,
                penanya: nama,
                pertanyaan: data.pertanyaan,
                url: window.location.origin
            })
            await LoadingTimer(request.data.message, "success", 3000)
        } catch (error:any) {
            let msg
            if (error?.response?.data?.message) {
                msg = error.response.data.message
            } else {
                msg = error.message
            }
            await LoadingTimer(msg, "error", 3000)
        }
        setShowModal(false);
    }
    return (
        <React.Fragment>
            <section id="team" className="team section-bg">
                {loading && (
                    <div id="loading" className="loading-container">
                        <Spinner text={""}/>
                    </div>
                )}
                <div className="container" data-aos="fade-up">
                    <div className="breadcrumbs" data-aos="fade-in">
                        <div className="section-title">
                            <h2>Tanya Jawab Fiqih</h2>
                        </div>
                        <div className="container mt-3">
                            <div className="row justify-content-center">
                                <div className="col-md-6">
                                    <form id="search-form">
                                        <div className="input-group mb-3">
                                            <label htmlFor="search-input"></label>
                                            <input type="text" className="form-control"
                                                   placeholder="Cari Pertanyaan"
                                                   name="cari"
                                                   id="search-input"
                                                   onChange={(event) => setKeyword(event.target.value)}
                                                   onKeyDown={handleKeyPress}/>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <center>
                                <button className="btn btn-success" onClick={() => {setShowModal(true)}}>Kirim Pertanyaan</button>
                            </center>
                        </div>
                    </div>
                    <Modal show={showModal} onHide={() => {setShowModal(false)}} centered={true} size={"xl"}>
                        <Modal.Header closeButton>
                            <Modal.Title>Kirim pertanyaan anda</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <div className="card-deck">
                                <div className="row justify-content-center">
                                    <form className="row g-3" onSubmit={handleSubmit(onSubmit)}>
                                        <div className="col-md-12 mb-3">
                                            <Input
                                                label="nama"
                                                register={fdata}
                                                required={false}
                                                placeholder="Nama (Opsional)"
                                                defaultValue=""/>
                                        </div>
                                        <div className="col-md-12 mb-3">
                                            <TextArea
                                                label="pertanyaan"
                                                register={fdata}
                                                required={true}
                                                placeholder="Pertanyaan (Maksimal 200 Karakter)"
                                                defaultValue=""/>
                                        </div>
                                        <div className="text-center">
                                            <Button variant="secondary" onClick={() => {setShowModal(false)}} style={{margin: 3}} >
                                                Batal
                                            </Button>
                                            <Button variant="success" type="submit" style={{margin: 3}}>
                                                Kirim
                                            </Button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </Modal.Body>
                    </Modal>
                    {data && data.length === 0 ? (
                        <div className="row">
                            <div className="col-md-12" id="no-data-found">
                                <div className="alert alert-danger">Data tidak ditemukan</div>
                            </div>
                        </div>
                    ) : (
                        <div className="row">
                            <div className="container">
                                <div className="mt-3">
                                    {data.map((item: any, index: number) => (
                                        <TanyaJawab
                                            key={index}
                                            data={item}
                                        />
                                    ))}
                                </div>
                                <div className="d-flex justify-content-center mt-3">
                                    {data.length > 0 && (
                                        <div className="d-flex justify-content-center mt-3">
                                            <Pagination
                                                currentPage={currentPage}
                                                itemsPerPage={pagedata[0]}
                                                totalItems={pagedata[1]}
                                                onPageChange={handlePageChange}
                                            />
                                        </div>
                                    )}
                                </div>
                            </div>
                        </div>
                    )}
                </div>
            </section>
        </React.Fragment>
    );
};

async function getData(page: number = 1, cari: string = "") {
    const api = process.env["NEXT_PUBLIC_API_URL"];
    let data: {};
    if (cari === "") {
        data = {
            page: page,
        };
    } else {
        data = {
            page: page,
            keyword: cari,
        };
    }
    const res = await axios.post(`${api}/api/tanya_jawab`, data);
    return await res.data;
}

export default Index;
