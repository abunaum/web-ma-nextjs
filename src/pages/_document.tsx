import {Html, Head, Main, NextScript} from 'next/document'
import Script from "next/script"

export default function Document() {
    return (
        <Html lang="en">
            <Head>
                {/*/!*<meta content="width=device-width, initial-scale=1.0" name="viewport"/>*!/*/}
                {/* eslint-disable-next-line @next/next/next-script-for-ga */}
                <Script async
                        src={`https://www.googletagmanager.com/gtag/js?id=${process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS}`}/>
                <Script id="google-analytics">
                    {`
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
 
          gtag('config', '${process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS}');
        `}
                </Script>
            </Head>
            <body>
            <Main/>
            <NextScript/>
            </body>
        </Html>
    )
}
