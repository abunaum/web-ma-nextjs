import Image from "next/image";
import React from "react";

interface Intra_CardProps {
    data: any[];
}

export default function Intra_Card({ data }: Intra_CardProps) {
    return (
        <>
            {data.map((item: any, key: number) => (
                <div className="col-md-auto d-flex align-items-stretch mb-3" data-aos="zoom-in" data-aos-delay="100" key={key}>
                    <div className="card p-1">
                        <div className="view overlay">
                            <Image
                                src={item.image}
                                alt="Card image cap"
                                className="card-img-top"
                                width={800}
                                height={800}
                                priority
                                style={{
                                    height: "20em",
                                    width: "20em"
                                }}
                            />
                            <a href="#">
                                <div className="mask rgba-white-slight"></div>
                            </a>
                        </div>

                        <div className="card-body">
                            <div className="align-items-center d-flex justify-content-center">
                                <h2 className="card-title align-content-center">{item.nama}</h2>
                            </div>
                        </div>
                    </div>
                </div>
            ))}
        </>
    );
}