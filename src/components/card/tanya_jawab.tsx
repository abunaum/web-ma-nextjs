import React from 'react';
import styles from "@/styles/card/tanya_jawab.module.css";
import {Icon} from "@iconify/react";
import {LoadingTimer} from "@/components/waitLoading";

type TanyaJawabProps = {
    data: {
        slug: string,
        pertanyaan: string,
        penanya: string,
        jawaban?: string,
        penjawab?: string,
        created_at: string,
    }
}
const TanyaJawab: React.FC<TanyaJawabProps> = ({data}) => {
    const {slug, pertanyaan, penanya, penjawab, created_at} = data;
    const perbaikan = () => {
        LoadingTimer("Fitur ini sedang dalam pengembangan", "info", 1500);
    }
    return (
        <React.Fragment>
            <div className={`${styles.card_div} row`} onClick={perbaikan}>
                <div className="col-lg-2">
                    <div className={styles.card_image}>
                        <Icon icon="ph:seal-question-bold" color="#37517e" width="128" height="128"/>
                    </div>
                </div>
                <div className="col-lg-10">
                    <div className={styles.card_data}>
                        <div className={styles.card_body}>
                            <div className="row">
                                <p className={styles.card_title}>{pertanyaan}</p>
                                <div className={styles.card_foot}>
                                    <p className={styles.foot_text}>Tanggal: {created_at}</p>
                                    <p className={styles.foot_text}>Penanya: {penanya}</p>
                                    <p className={styles.foot_text}>Penjawab: {penjawab}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
};

export default TanyaJawab;