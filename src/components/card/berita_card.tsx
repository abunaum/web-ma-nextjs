import {CldImage} from 'next-cloudinary';
import React from "react";
import moment from "moment";
import 'moment/locale/id';
import Link from "next/link";

interface GS_CardProps {
    data: any[];
}

moment().locale('id');
export default function Card_Berita({data}: GS_CardProps) {
    let prioritas: any[] = [];
    let sekunder: any[] = [];
    data.map((item: any, key: number) => {
        if (key === 0) {
            prioritas.push(item);
        } else {
            sekunder.push(item);
        }
    });
    return (
        <>
            {prioritas.map((item: any, key: number) => (
                <div key={key}>
                    <div className="card mb-3">
                        <div style={{maxHeight: "450px", overflow: "hidden"}}>
                            <CldImage
                                width="1200"
                                height="900"
                                className="img-thumbnail"
                                src={item.gambar}
                                alt="gambar"
                                priority
                                style={{height: '30em', width: '45em'}}
                            />
                        </div>
                        <div className="card-body">
                            <div style={{textAlign: "center"}}>
                                <h2 className="card-title" id="card-judul-1">{item.judul}</h2>
                                <div className="card-text mb-3">
                                    <small className="text-muted">
                                        Diposting : {moment(item.created_at).format('D MMMM YYYY')}.
                                    </small>
                                    <br/>
                                    <small className="text-muted" id="card-kategori-1">
                                        Kategori : {item.kategori}.
                                    </small>
                                    <br/>
                                    <small className="text-muted" id="card-author-1">
                                        By : {item.author}.
                                    </small>
                                </div>
                            </div>

                            <p className="card-text" id="card-excerpt-1">{item.excerpt}</p>
                            <center>
                                <Link
                                    href={{
                                        pathname: `/berita/${item.slug}`
                                    }}
                                    className="btn btn-primary mb-3"
                                >
                                    Baca Selengkapnya
                                </Link>
                            </center>
                        </div>
                    </div>
                </div>
            ))}
            <div className="row">
                {sekunder.map((item: any, key: number) => (
                    <div className="col-md-4 mb-3" key={key}>
                        <div className="card" data-aos="fade-in" style={{height: "50rem"}}>
                            <div style={{maxHeight: "250px", overflow: "hidden"}}>
                                <CldImage
                                    width="1200"
                                    height="900"
                                    className="img-thumbnail"
                                    src={item.gambar}
                                    alt="gambar"
                                    priority
                                    style={{height: '270px', width: 'auto'}}
                                />
                            </div>
                            <div className="card-body">
                                <h5 className="card-title" id="card-judul">{item.judul}</h5>
                                <p className="ql-align-justify" id="card-excerpt">{item.excerpt}</p>
                                <Link
                                    href={{
                                        pathname: `/berita/${item.slug}`
                                    }}
                                    className="btn btn-primary mb-3"
                                >
                                    Baca Selengkapnya
                                </Link>
                                <div className="card-text text-muted card-tanggal">
                                    <small className="text-muted">
                                        <p>Diposting : {moment(item.created_at).format('D MMMM YYYY')}.</p>
                                    </small>
                                    <small className="text-muted">
                                        Kategori : {item.kategori}
                                    </small>
                                    <small className="text-muted">
                                        &nbsp;|&nbsp;
                                    </small>
                                    <small className="text-muted">
                                        By : {item.author}
                                    </small>
                                </div>
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        </>
    );
}