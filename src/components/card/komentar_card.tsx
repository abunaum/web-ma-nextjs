import React from 'react';
import {CommentTypes} from "@/utils/type/comment";
import comment from "@/styles/card/comment.module.css";
import Image from "next/image";
import moment from "moment";
import 'moment/locale/id';
import {Icon} from '@iconify/react';
import Swal from "sweetalert2";
import axios from "axios";
import {LoadingTimer, showWaitLoading} from "@/components/waitLoading";

type KomentarCardTypes = {
    data: CommentTypes[],
    postid: string,
    mutate: () => void,
    session?: any
}
const KomentarCard = ({data, postid, mutate, session}: KomentarCardTypes) => {
    const handleDelete = (id: string | undefined, komentar: string | undefined) => async () => {
        Swal.fire({
            title: 'Apakah anda yakin?',
            html: `Anda tidak dapat mengembalikan komentar yang sudah dihapus!<br/>"${komentar}"`,
            icon: 'warning',
            showCancelButton: true,
            cancelButtonText: 'Batal',
            confirmButtonText: 'Ya, hapus!',
            confirmButtonColor: '#06a914',
            cancelButtonColor: '#c99b05'
        }).then(async (result) => {
            if (result.isConfirmed) {
                showWaitLoading('Mohon tunggu sebentar, menghapus komentar ...');
                await prosesDelete(postid, id);
            }
        });
    }
    const prosesDelete = async (postid: string, id: string | undefined) => {
        if (!id) {
            return
        }
        const api = process.env.NEXT_PUBLIC_API_URL;
        try {
            const hapus = await axios.delete(`${api}/api/post/${postid}/comment/${id}`);
            if (hapus.data.success) {
                mutate();
                await LoadingTimer(hapus.data.message, "success", 3000);
                return
            } else {
                await LoadingTimer(hapus.data.message, "error", 3000);
                return
            }
        } catch (error: any) {
            let msg: string;
            if (typeof error.response?.data?.message !== 'undefined') {
                msg = error.response.data.message;
            } else {
                msg = error.message;
            }
            await LoadingTimer(msg, "error", 3000);
            return
        }
    }
    if (data.length > 0) {
        return (
            <React.Fragment>
                {data
                    .sort((a: CommentTypes, b: CommentTypes) => new Date(b.created_at).getTime() - new Date(a.created_at).getTime())
                    .map((komentar: CommentTypes, index: number) => (
                        <div className="card mb-3" key={index}>
                            <div className="card-body">
                                <div className="row">
                                    <ul className="col-md-12">
                                        <li className={`${comment.box_result} row`}>
                                            <div className={`${comment.avatar_comment} col-md-1`}>
                                                <Image
                                                    src={komentar.userImage ?? ''}
                                                    width={50}
                                                    height={50}
                                                    alt="image"/>
                                            </div>
                                            <div className={`${comment.result_comment} col-md-11`}>
                                                <h4>{komentar.user ?? ''}</h4>
                                                <p>{komentar.komentar}</p>
                                                <div className={`${comment.tools_comment}`}>
                                                    <span>{moment(komentar.created_at).format("dddd, Do MMMM YYYY - HH:mm")}</span>
                                                    {session && session?.user?.email === komentar.userEmail &&(
                                                        <span>
                                                        &nbsp;|&nbsp;
                                                            <Icon
                                                                icon="fluent-mdl2:delete"
                                                                color="red"
                                                                width="16"
                                                                height="16"
                                                                style={{cursor: "pointer"}}
                                                                onClick={handleDelete(komentar.id, komentar.komentar)}
                                                            />
                                                    </span>
                                                    )}
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    ))}
            {/*    session?.user?.email === data?.userEmail*/}
            </React.Fragment>
        )
    } else {
        return (
            <React.Fragment>
            </React.Fragment>
        )
    }
};

export default KomentarCard;