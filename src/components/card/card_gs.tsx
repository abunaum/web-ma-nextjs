import {Icon} from "@iconify/react";
import Swal from "sweetalert2";
import {CldImage} from 'next-cloudinary';
import styles from '@/styles/card/person.module.css';

interface GS_CardProps {
    data: any[];
}

export default function Card_GS({data}: GS_CardProps) {
    return (
        <>
            {data.map((item: any, key: number) => (
                <div className="col-lg-3 m-3" key={key}>
                    <div className={`${styles.member} rowalign-items-start`} data-aos="zoom-in" data-aos-delay="100"
                         style={{height: "30em"}}>
                        <div style={{display: 'flex', justifyContent: 'center'}}>
                            <div className={styles.photo_rounded}>
                                <CldImage
                                    width="600"
                                    height="600"
                                    src={item.profile.image}
                                    alt="gambar"
                                    style={{width: '11.5em', height: '11.5em'}}
                                    priority
                                />
                            </div>
                        </div>
                        <div className="mt-3">
                            <h4 style={{textAlign: "center"}}>{item.nama}</h4>
                            {item.bidang_studi ? (
                                <>
                                    <p>Jabatan : {item.jabatan ?? '-'}</p>
                                    <span>Bidang Studi : {item.bidang_studi}</span>
                                </>
                            ) : (
                                <>
                                    <p>&nbsp;</p>
                                    <p>Jabatan : {item.jabatan ?? '-'}</p>
                                    <span>&nbsp;</span>
                                </>
                            )}
                            <p>No HP : {item.no_hp ?? '-'}</p>
                            <hr/>
                            <div className={styles.social} style={{justifyContent: "center"}}>
                                <div onClick={() => {
                                    const username: string = item.profile.telegram ?? '';
                                    sosial('telegram', username);
                                }}>
                                    <Icon icon="logos:telegram" width="32" height="32" className="m-2"/>
                                </div>
                                <div onClick={() => {
                                    const username: string = item.profile.facebook ?? '';
                                    sosial('facebook', username);
                                }}>
                                    <Icon icon="logos:facebook" width="32" height="32" className="m-2"/>
                                </div>
                                <div onClick={() => {
                                    const username: string = item.profile.instagram ?? '';
                                    sosial('instagram', username);
                                }}>
                                    <Icon icon="skill-icons:instagram" width="32" height="32" className="m-2"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            ))}
        </>);
}

function sosial(jenis: string, link: string) {
    switch (jenis) {
        case 'telegram':
            if (link !== '') {
                window.open(`https://t.me/${link}`, '_blank');
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Akun Telegram belum tersedia!',
                    showConfirmButton: false,
                    timer: 1500
                })
            }
            break;
        case 'facebook':
            if (link !== '') {
                window.open(`https://facebook.com/${link}`, '_blank');
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Akun Facebook belum tersedia!',
                    showConfirmButton: false,
                    timer: 1500
                })
            }
            break;
        case 'instagram':
            if (link !== '') {
                window.open(`https://instagram.com/${link}`, '_blank');
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Akun Instagram belum tersedia!',
                    showConfirmButton: false,
                    timer: 1500
                })
            }
            break;
        default:
            break;
    }
}