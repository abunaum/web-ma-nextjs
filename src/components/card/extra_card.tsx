import Image from "next/image";
import React from "react";

interface Extra_CardProps {
    data: any[];
}

export default function Extra_Card({ data }: Extra_CardProps) {
    return (
        <>
            {data.map((item: any, key: number) => (
                <div className="col-md-auto col-12 d-flex align-items-stretch mb-4" data-aos="zoom-in" data-aos-delay="100" key={key}>
                    <div className="card p-1">
                        <div className="view overlay">
                            <div className="col-12">
                                <div className="view overlay">
                                    <Image
                                        src={item.image}
                                        alt="Card image cap"
                                        className="img-thumbnail"
                                        width={800}
                                        height={800}
                                        priority
                                        style={{
                                            height: "20em",
                                            width: "20em"
                                        }}
                                    />
                                    <a href="#">
                                        <div className="mask rgba-white-slight"></div>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div className="card-body">
                            <div className="align-items-center d-flex justify-content-center">
                                <h3 className="card-title align-content-center">{item.title}</h3>
                            </div>
                        </div>
                    </div>
                </div>
            ))}
        </>
    );
}