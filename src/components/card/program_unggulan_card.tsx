import Image from "next/image";
import React from "react";
import {Button, Modal} from "react-bootstrap";
import styles from '@/styles/pages/program.module.css';
import { Icon } from '@iconify/react';

interface ProgramUnggulanCardProps {
    data: any[];
}

export default function ProgramUnggulanCard({data}: ProgramUnggulanCardProps) {
    const [showModal, setShowModal] = React.useState<boolean>(false);
    const [selectedItem, setSelectedItem] = React.useState<any | null>(null);
    const openModal = (item: any) => {
        setSelectedItem(item);
        setShowModal(true);
    };

    const closeModal = () => {
        setSelectedItem(null);
        setShowModal(false);
    };
    return (
        <>
            {data.map((item: any, key: number) => (
                <div className="col-xl-4 col-md-6 d-flex align-items-stretch mb-3" data-aos="zoom-in"
                     data-aos-delay="100" key={key}>
                    <div className="card p-1">
                        <div className="view overlay">
                            <Image
                                src={item.image}
                                alt="Card image cap"
                                className={styles.unggulan}
                                width={900}
                                height={900}
                                priority
                                style={{height: "18em",width: "18em"}}
                            />
                            <a href="#">
                                <div className="mask rgba-white-slight"></div>
                            </a>
                        </div>
                        <div className="card-body">
                            <div className="align-items-center d-flex justify-content-center">
                                <h4 className="card-title align-content-center">{item.nama}</h4>
                            </div>
                            <p className="card-text">{item.text}</p>
                        </div>
                        {item.nama === "Prodistik" && (
                            <div className="card-footer" onClick={() => openModal(item)}>
                                <a href="#" className="btn btn-primary">
                                    <i className="bx bx-link"></i>
                                    <span>Lihat Pilihan</span>
                                </a>
                            </div>
                        )}
                    </div>
                </div>
            ))}
            <Modal show={showModal} onHide={closeModal} centered={true} size={"xl"}>
                <Modal.Header closeButton>
                    <Modal.Title>{selectedItem?.nama}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="card-deck">
                        <div className="row justify-content-center">
                            <div className={`${styles.card__bx} ${styles.card__1}`}>
                                <div className={styles.card__data}>
                                    <div className={styles.card__icon}>
                                        <div className={styles.card__iconBx}>
                                            <Icon icon="ion:code-slash-sharp" width="128" height="128" />
                                            {/*<i className="fa-solid fa-pen-ruler"></i>*/}
                                        </div>
                                    </div>
                                    <div className={styles.card__content}>
                                        <h3>Programming</h3>
                                        <p>Kode yang mengubah ide menjadi teknologi, memungkinkan kita menciptakan masa depan dengan kode, dan menggabungkan logika dengan imajinasi untuk membangun dunia digital.</p>
                                    </div>
                                </div>
                            </div>
                            <div className={`${styles.card__bx} ${styles.card__2}`}>
                                <div className={styles.card__data}>
                                    <div className={styles.card__icon}>
                                        <div className={styles.card__iconBx}>
                                            <Icon icon="arcticons:youcutvideoeditor" width="128" height="128" />
                                        </div>
                                    </div>
                                    <div className={styles.card__content}>
                                        <h3>Video Editor</h3>
                                        <p>Seni menggabungkan gambar, suara, dan efek visual untuk menciptakan kisah yang memukau. Ini adalah kreativitas dalam gerakan, waktu, dan suara yang mengubah momen menjadi karya seni.</p>
                                    </div>
                                </div>
                            </div>
                            <div className={`${styles.card__bx} ${styles.card__3}`}>
                                <div className={styles.card__data}>
                                    <div className={styles.card__icon}>
                                        <div className={styles.card__iconBx}>
                                            <Icon icon="ic:outline-camera" width="128" height="128" />
                                        </div>
                                    </div>
                                    <div className={styles.card__content}>
                                        <h3>Graphics Design</h3>
                                        <p>Seni komunikasi visual yang mengubah ide menjadi pesan estetis, menciptakan identitas, dan memanipulasi persepsi melalui kreativitas terstruktur.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={closeModal}>
                        Tutup
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}