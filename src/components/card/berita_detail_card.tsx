import React from 'react';
import {CldImage} from "next-cloudinary";
import moment from "moment/moment";
import {Icon} from "@iconify/react";
import Link from "next/link";

type BeritaDetailCardTypes = {
    data: any,
    urlshare: string,
    urlgambar: string,
}
const BeritaDetailCard = ({data, urlshare, urlgambar}:BeritaDetailCardTypes) => {
    return (
        <div className="card mb-3">
            <div id="imagediv">
                {data.gambar && data.gambar !== "website/post/nvslram21uuo8cdlyy0g" ? (
                    <CldImage
                        width="1200"
                        height="900"
                        className="img-thumbnail"
                        src={data.gambar}
                        alt="gambar"
                        priority
                        style={{height: 'auto', width: '100%'}}
                    />
                ) : (
                    <React.Fragment></React.Fragment>
                )}
            </div>
            <div className="card-body">
                <center>
                    <h2 className="card-title">{data.judul}</h2>
                    <p className="card-text mb-3">
                        <small className="text-muted">
                            Diposting : {moment(data.created_at).format('D MMMM YYYY')}
                            <br/>
                            By {data.author} | Kategori : {data.kategori}
                        </small>
                    </p>
                </center>
                <div
                    className="card-text"
                    dangerouslySetInnerHTML={{__html: data.body}}
                    style={{textAlign: "justify"}}
                />
                <center>
                    <div className="mt-3 sharethis-inline-share-buttons">
                        <p>Bagikan : </p>
                        <div className="social-links mt-3">
                            <a href={`https://telegram.me/share/url?url=${urlshare}&text=${data.judul}`}
                               target="_blank">
                                <Icon icon="logos:telegram" width="32" style={{padding: 3}}/>
                            </a>
                            <a href={`https://www.facebook.com/sharer/sharer.php?u=${urlshare}&t=${data.judul}`}
                               target="_blank">
                                <Icon icon="logos:facebook" width="32" style={{padding: 3}}/>
                            </a>
                            <a href={`https://api.whatsapp.com/send?text=${data.judul}%20${urlshare}`}
                               target="_blank">
                                <Icon icon="logos:whatsapp-icon" width="32" style={{padding: 3}}/>
                            </a>
                            <a href={`https://twitter.com/intent/tweet?text=${data.judul}&url=${urlshare}`}
                               target="_blank">
                                <Icon icon="skill-icons:twitter" width="32" style={{padding: 3}}/>
                            </a>
                            <a href={`https://www.linkedin.com/shareArticle?token&isFramed=false&url=${urlshare}`}
                               target="_blank">
                                <Icon icon="skill-icons:linkedin" width="32" style={{padding: 3}}/>
                            </a>
                            <a href={`https://id.pinterest.com/pin-builder/?description=${data.judul}&media=${urlgambar}&method=button&url=${urlshare}`}
                               target="_blank">
                                <Icon icon="logos:pinterest" width="32" style={{padding: 3}}/>
                            </a>
                        </div>
                    </div>
                </center>
                <hr className="mb-3"/>
                <center>
                    <Link
                        href={{
                            pathname: `/berita`
                        }}
                        className="btn btn-primary mb-3"
                    >
                        Kembali ke semua berita
                    </Link>
                </center>
            </div>
        </div>
    );
};

export default BeritaDetailCard;