import React from 'react';
import comment from "@/styles/card/comment.module.css";
import Image from "next/image";
import {signOut, useSession} from "next-auth/react";
import {LoadingTimer, showWaitLoading} from "@/components/waitLoading";
import {buatKomentar, kirimKomentar} from "@/utils/postingan/kirim_komentar";

type BuatKomentarCardTypes = {
    postid: string
    mutate: () => void
}
const BuatKomentarCard = ({postid, mutate}:BuatKomentarCardTypes) => {
    const {data: session} = useSession();
    const [commentnya, setComment] = React.useState<string>('');
    const [isSending, setIsSending] = React.useState<boolean>(false);
    const handleKirim = async () => {
        setIsSending(true);
        showWaitLoading('Mohon tunggu sebentar, mencoba mengirim komentar anda ...');
        const buat = await buatKomentar(commentnya, session)
        if (!buat.success) {
            await LoadingTimer(buat.message, "error", 3000);
            return
        }
        const kirim = await kirimKomentar(postid, buat.data);
        if (!kirim.success) {
            await LoadingTimer(kirim.message, "error", 3000);
            return
        } else {
            mutate();
            setComment('');
            const input = document.getElementById("commentar") as HTMLInputElement;
            input.value = '';
            await LoadingTimer(kirim.message, "success", 3000);
            setIsSending(false);
            return
        }
    }
    return (
        <React.Fragment>
            <div className="card">
                <div className="card-body">
                    <h5 className="card-title">Buat Komentar</h5>
                    <div className="card-text">
                        <div className={comment.body_comment}>
                            <div className="row">
                                <div className={`${comment.avatar_comment} col-md-1`}>
                                    <Image
                                        src={session?.user?.image ?? ''}
                                        width={50}
                                        height={50}
                                        alt="image"/>
                                </div>
                                <div className={`${comment.box_comment} col-md-11`}>
                                                            <textarea
                                                                id="commentar"
                                                                className={`${comment.commentar}`}
                                                                placeholder="Komentar anda ..."
                                                                onChange={(e) => {
                                                                    return setComment(e.target.value);
                                                                }}>
                                                            </textarea>
                                    <div className={`${comment.box_post}`}>
                                        <div className={`${comment.pull_left}`}>
                                            <button
                                                className="btn btn-success"
                                                onClick={handleKirim}
                                                disabled={isSending}>Kirim
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
                <button className="btn btn-danger" onClick={() => signOut()}>Keluar dari sesi
                    saat ini
                </button>
            </div>
        </React.Fragment>
    );
};

export default BuatKomentarCard;