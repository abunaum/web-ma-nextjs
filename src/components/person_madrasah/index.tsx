import React from "react";
import axios from "axios";
import Card_GS from "@/components/card/card_gs";
import Pagination from "@/components/pagination/pagination";
import Spinner from "@/components/spinner";

interface Interface {
    nama: string;
    url: string;
}
export default function PersonCategory({tipe}: { tipe: Interface}) {
    const [data, setData] = React.useState<any[]>([]);
    const [pagedata, setPageData] = React.useState<number[]>([]);
    const [loading, setLoading] = React.useState<boolean>(true);
    const [currentPage, setCurrentPage] = React.useState<number>(1);
    const [keyword, setKeyword] = React.useState<string>("")
    React.useEffect(() => {
        fetchData(1, keyword).then(() => {
            console.log("Data telah diambil");
        });
    }, [keyword]);
    const fetchData = async (page: number, searchKeyword: string) => {
        setLoading(true);
        const result = await getData(page, searchKeyword);
        setData(result.data);
        setPageData([
            result.length ?? 0,
            result.recordsTotal ?? 0,
            result.recordsFiltered ?? 0,
            result.start ?? 0,
        ]);
        setLoading(false);
    };

    const handleSearch = (searchKeyword: string) => {
        setKeyword(searchKeyword);
    };

    const handlePageChange = (page: number) => {
        setCurrentPage(page);
        fetchData(page, keyword).then(() => {
            console.log("Data telah diambil");
        });
    };
    const handleKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (event.key === 'Enter') {
            event.preventDefault();
            const searchKeyword: string = (document.getElementById("search-input") as HTMLInputElement).value;
            handleSearch(searchKeyword);
        }
    };

    async function getData(page: number = 1, cari: string = "") {
        const api = process.env["NEXT_PUBLIC_API_URL"];
        let data: {};
        if (cari === "") {
            data = {
                page: page,
            };
        } else {
            data = {
                page: page,
                keyword: cari,
            };
        }
        const res = await axios.post(`${api}/${tipe.url}`, data);
        return await res.data;
    }
    return (
        <>
            <section className="section-bg">
                {loading && (
                    <div id="loading" className="loading-container">
                        <Spinner text={""}/>
                    </div>
                )}
                <div className="container" data-aos="fade-up" style={{marginTop: "-120px"}}>
                    <div className="breadcrumbs" data-aos="fade-in">
                        <div className="section-title">
                            <h2>{tipe.nama}</h2>
                        </div>
                        <div className="container mt-3">
                            <div className="row justify-content-center">
                                <div className="col-md-6">
                                    <form id="search-form">
                                        <div className="input-group mb-3">
                                            <label htmlFor="search-input"></label>
                                            <input type="text" className="form-control"
                                                   placeholder={`Cari ${tipe.nama}...`}
                                                   name="cari"
                                                   id="search-input"
                                                   onChange={(event) => setKeyword(event.target.value)}
                                                   onKeyPress={handleKeyPress}/>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    {!loading && data.length === 0 ? (
                        <div className="row">
                            <div className="col-md-12" id="no-data-found">
                                <div className="alert alert-danger">Data tidak ditemukan</div>
                            </div>
                        </div>
                    ) : (
                        <div className="row">
                            <div className="container">
                                <div className="row" style={{justifyContent: "center"}}>
                                    <Card_GS data={data}/>
                                </div>
                                <div className="d-flex justify-content-center mt-3">
                                    {data.length > 0 && (
                                        <div className="d-flex justify-content-center mt-3">
                                            <Pagination
                                                currentPage={currentPage}
                                                itemsPerPage={pagedata[0]}
                                                totalItems={pagedata[1]}
                                                onPageChange={handlePageChange}
                                            />
                                        </div>
                                    )}
                                </div>
                            </div>
                        </div>
                    )}
                </div>
            </section>
        </>
    )
}