import Image from "next/image";
import Link from "next/link";
import React from "react";
import styles from "@/styles/homepage/hero.module.css";

export default function Hero() {
    return (
        <section className={`${styles.hero} d-flex align-items-center`}>
            <div className={`container ${styles.container} mb-5`}>
                <div className="row">
                    <div
                        className="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1 mb-8"
                        data-aos="fade-up" data-aos-delay="200">
                        <h1 className={styles.h1}>Madrasah Aliyah</h1>
                        <h1 className={styles.h1}>Zainul Hasan 1 Genggong</h1>
                        <h2 className={styles.h2}>Madrasah Berbasis Pesantren dan Teknologi</h2>
                        <div className="d-flex justify-content-center justify-content-lg-start">
                            <Link
                                href={{
                                    pathname: '/berita',
                                }}
                                className={`${styles.btn_get_started} scrollto`}
                                style={{textDecoration: "none"}}
                            >
                                Berita
                            </Link>
                            <a href="https://www.youtube.com/watch?v=cWrqxdyQZgc" className={`glightbox ${styles.btn_watch_video}`} target="_blank"
                               style={{textDecoration: "none"}}>
                                <i className="bi bi-play-circle"></i><span>Lihat Kami</span></a>
                        </div>
                    </div>
                    <div className={`col-lg-6 order-1 order-lg-2 ${styles.hero_img}`} data-aos="zoom-in" data-aos-delay="200" style={{textAlign: "center"}}>
                        <Image
                            src="/assets/img/LOGO.png"
                            alt="Logo Animasi"
                            className={`img-fluid ${styles.animated}`}
                            width={500}
                            height={300}
                            priority
                            style={{height: '50vh', width: 'auto'}}
                        />
                    </div>
                </div>
            </div>
        </section>
    )
}