import Image from "next/image";
import {useState} from "react";
import styles from "@/styles/homepage/pilih_kami.module.css"

export default function Pilih_kami() {
    const [activeAccordion, setActiveAccordion] = useState("accordion-list-1");

    const handleAccordionClick = (id: string) => {
        setActiveAccordion(id);
    };
    return (
        <section className={`${styles.why_us} section-bg`}>
            <div className="container-fluid" data-aos="fade-up">
                <div className="row">
                    <div className="col-lg-7 d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1">
                        <div className={styles.content}>
                            <h3>Mengapa harus sekolah di <strong>Madrasah Aliyah Zainul Hasan 1</strong></h3>
                        </div>
                        <div className={styles.accordion_list}>
                            <ul>
                                <li>
                                    <a
                                        data-bs-toggle="collapse"
                                        data-bs-target="#accordion-list-1"
                                        className={activeAccordion === "accordion-list-1" ? "" : "collapsed"}
                                        onClick={() => handleAccordionClick("accordion-list-1")}
                                    >
                                        <span>1</span>
                                        Pembelajaran Berbasis Teknologi
                                        <i className={`bx bx-chevron-down ${styles.icon_show}`}></i>
                                        <i className={`bx bx-chevron-up ${styles.icon_close}`}></i>
                                    </a>
                                    <div
                                        id="accordion-list-1"
                                        className={`collapse ${activeAccordion === "accordion-list-1" ? "show" : ""}`}
                                        data-bs-parent=".accordion-list"
                                    >
                                        <p>
                                            Dengan adanya program unggulan &#34;PRODISTIK&#34;, siswa akan mendapatkan
                                            pembelajaran
                                            berbasis teknologi.
                                        </p>
                                    </div>
                                </li>

                                <li>
                                    <a
                                        data-bs-toggle="collapse"
                                        data-bs-target="#accordion-list-2"
                                        className={activeAccordion === "accordion-list-2" ? "" : "collapsed"}
                                        onClick={() => handleAccordionClick("accordion-list-2")}
                                    >
                                        <span>2</span>
                                        Pendalaman Ilmu Agama
                                        <i className={`bx bx-chevron-down ${styles.icon_show}`}></i>
                                        <i className={`bx bx-chevron-up ${styles.icon_close}`}></i>
                                    </a>
                                    <div
                                        id="accordion-list-2"
                                        className={`collapse ${activeAccordion === "accordion-list-2" ? "show" : ""}`}
                                        data-bs-parent=".accordion-list"
                                    >
                                        <p>
                                            Dengan adanya program unggulan &#34;Tahqiqu Qiroatil Kutub dan Tahfidhul
                                            Quran&#34;,
                                            para siswa akan di perdalam ilmu agamanya.
                                        </p>
                                    </div>
                                </li>

                                <li>
                                    <a
                                        data-bs-toggle="collapse"
                                        data-bs-target="#accordion-list-3"
                                        className={activeAccordion === "accordion-list-3" ? "" : "collapsed"}
                                        onClick={() => handleAccordionClick("accordion-list-3")}
                                    >
                                        <span>3</span>
                                        Kegiatan Pesantren
                                        <i className={`bx bx-chevron-down ${styles.icon_show}`}></i>
                                        <i className={`bx bx-chevron-up ${styles.icon_close}`}></i>
                                    </a>
                                    <div
                                        id="accordion-list-3"
                                        className={`collapse ${activeAccordion === "accordion-list-3" ? "show" : ""}`}
                                        data-bs-parent=".accordion-list"
                                    >
                                        <p>
                                            Para siswa di MA Zainul Hasan 1 diwajibkan untuk masuk pesantren,
                                            sehingga
                                            parasiswa akan mendapatkan kegiatan pesantren.
                                        </p>
                                    </div>
                                </li>

                            </ul>
                        </div>

                    </div>

                    <div className={`col-lg-5 align-items-stretch order-1 order-lg-2 ${styles.img}`}>
                        <Image
                            src="/assets/img/why.png"
                            alt="Logo Animasi"
                            className="img-fluid"
                            width={500}
                            height={300}
                            priority
                            style={{height: 'auto', width: 'auto'}}
                        />
                    </div>
                </div>

            </div>
        </section>
    );
}