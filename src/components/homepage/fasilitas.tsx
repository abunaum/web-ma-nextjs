import { Icon } from '@iconify/react';
import styles from "@/styles/homepage/fasilitas.module.css"
export default function Fasilitas() {
    return (
        <section className={`${styles.services} section-bg`} style={{backgroundColor: '#ffffff'}}>
            <div className="container" data-aos="fade-up">

                <div className="section-title">
                    <h2>Fasilitas Madrasah</h2>
                    <p>Dengan adanya fasilitas madrasah yang memadai tentunya akan membuat kegiatan belajar mengajar
                        akan terasa lebih nyaman dan efektif.</p>
                </div>

                <div className="row">
                    <div className="col-xl-4 col-md-6 d-flex align-items-stretch" data-aos="zoom-in"
                         data-aos-delay="100">
                        <div className={styles.icon_box}>
                            <center>
                                <div className={styles.icon}>
                                    <Icon icon="maki:town-hall" width="32" height="32" />
                                </div>
                                <h4>AULA</h4>
                            </center>
                            <p>Digunakan ketika ada kegiatan penting di madrasah aliyah zainul hasan 1 genggong</p>
                        </div>
                    </div>

                    <div className="col-xl-4 col-md-6 d-flex align-items-stretch mt-4 mt-md-0" data-aos="zoom-in"
                         data-aos-delay="200">
                        <div className={styles.icon_box}>
                            <center>
                                <div className={styles.icon}>
                                    <Icon icon="openmoji:desktop-computer" width="32" height="32" />
                                </div>
                                <h4>LAB Komputer</h4>
                            </center>
                            <p>Digunakan ketika mata pelajaran yang berkaitan dengan komputer.</p>
                        </div>
                    </div>

                    <div className="col-xl-4 col-md-6 d-flex align-items-stretch mt-4 mt-xl-0" data-aos="zoom-in"
                         data-aos-delay="400">
                        <div className={styles.icon_box}>
                            <center>
                                <div className={styles.icon}>
                                    <Icon icon="game-icons:materials-science" width="32" height="32" />
                                </div>
                                <h4>LAB IPA</h4>
                            </center>
                            <p>Digunakan ketika mata pelajaran yang berkaitan dengan ipa.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}