import { useTypewriter, Cursor} from "react-simple-typewriter";
import styles from "@/styles/homepage/arab.module.css";
export default function Text_Arab() {
    const [ text ] = useTypewriter({
        words: [
            "المدرسة العالية زين الحسن ١ قنقون"
        ],
        loop: 0,
        typeSpeed: 100,

    });
    return (
        <section className={`${styles.clients} ${styles.arab_bg}`}>
            <div className="container">
                <div className="row" data-aos="zoom-in">
                    <div className="col-lg-12 col-md-12 d-flex align-items-center justify-content-center">
                        <p className={styles.animated_arab} dir="rtl">
                            {text}
                        </p>
                    </div>
                </div>
            </div>
        </section>
    );
}