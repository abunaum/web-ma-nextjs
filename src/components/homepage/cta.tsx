import styles from "@/styles/homepage/cta.module.css"
export default function CTA() {
    return (
        <section className={styles.cta}>
            <div className="container" data-aos="zoom-in">
                <div className="row">
                    <div className="col-lg-9 text-center text-lg-start">
                        <h3>Ada pertanyaan lain?</h3>
                        <p> Jika ada hal lain yang ingin ditanyakan silahkan menghubungi kami.</p>
                    </div>
                        <div className={`col-lg-3 ${styles.cta_btn_container} text-center`}>
                        <a className={`${styles.cta_btn} align-middle`} href="https://t.me/najwannada" target="_blank" style={{textDecoration: "none"}}>Hubungi
                            kami</a>
                    </div>
                </div>

            </div>
        </section>
    );
}