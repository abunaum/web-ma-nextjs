import { Icon } from '@iconify/react';
import {useState} from "react";
import styles from "@/styles/homepage/faq.module.css"
export default function FAQ() {
    const [activeAccordion, setActiveAccordion] = useState("faq-list-1");

    const handleAccordionClick = (id: string) => {
        setActiveAccordion(id);
    };
    return (
        <section className={`${styles.faq} section-bg`} style={{backgroundColor: '#e7e6e6'}}>
            <div className="container" data-aos="fade-up">

                <div className="section-title">
                    <h2>Hal yang sering ditanyakan</h2>
                </div>
                <div className={styles.faq_list}>
                    <ul>
                        <li data-aos="fade-up" data-aos-delay="100">
                            <a
                                data-bs-toggle="collapse"
                                data-bs-target="#faq-list-1"
                                className={activeAccordion === "faq-list-1" ? "" : "collapsed"}
                                onClick={() => handleAccordionClick("faq-list-1")}
                                style={{ fontSize: '1.5rem' }}
                            >
                                <Icon icon="ph:seal-question-bold" color="#3fdede" width="32" height="32" /> Apa itu program &#34;Prodistik&#34; ?
                                <i className={`bx bx-chevron-down ${styles.icon_show}`}></i>
                                <i className={`bx bx-chevron-up ${styles.icon_close}`}></i>
                            </a>
                            <div
                                id="faq-list-1"
                                className={`collapse ${activeAccordion === "faq-list-1" ? "show" : ""}`}
                                data-bs-parent=".faq-list"
                            >
                                <p>
                                    Program D1 bidang Teknologi Informasi dan Komunikasi (PRODISTIK) adalah kerja sama
                                    MA
                                    ZAinul Hasan 1 Genggong dengan ITS Surabaya dalam bidang teknologi informasi dan
                                    komunikasi. Program ini bertujuan untuk meningkatkan kualitas sumber daya manusia di
                                    bidang teknologi informasi dan komunikasi.
                                </p>
                            </div>
                        </li>

                        <li data-aos="fade-up" data-aos-delay="200">
                            <a
                                data-bs-toggle="collapse"
                                data-bs-target="#faq-list-2"
                                className={activeAccordion === "faq-list-2" ? "" : "collapsed"}
                                onClick={() => handleAccordionClick("faq-list-2")}
                                style={{ fontSize: '1.5rem' }}
                            >
                                <Icon icon="ph:seal-question-bold" color="#3fdede" width="32" height="32" /> Apa itu program &#34;Tahqiqu Qiroatil Kutub&#34; ?
                                <i className={`bx bx-chevron-down ${styles.icon_show}`}></i>
                                <i className={`bx bx-chevron-up ${styles.icon_close}`}></i>
                            </a>
                            <div
                                id="faq-list-2"
                                className={`collapse ${activeAccordion === "faq-list-2" ? "show" : ""}`}
                                data-bs-parent=".faq-list"
                            >
                                <p>
                                    Program Tahqiqu Qiroatil Kutub adalah kelas unggulan untuk santri yang ingin
                                    mendalami
                                    khazanah ilmu keislaman, kajian kitab kuning, dan bahasa Arab. Program ini bekerja
                                    sama
                                    dengan Fakultas Humaniora Jurusan Bahasa dan Sastra Arab UIN Maulana Malik Ibrahim
                                    Malang.
                                </p>
                            </div>
                        </li>

                        <li data-aos="fade-up" data-aos-delay="300">
                            <a
                                data-bs-toggle="collapse"
                                data-bs-target="#faq-list-3"
                                className={activeAccordion === "faq-list-3" ? "" : "collapsed"}
                                onClick={() => handleAccordionClick("faq-list-3")}
                                style={{ fontSize: '1.5rem' }}
                            >
                                <Icon icon="ph:seal-question-bold" color="#3fdede" width="32" height="32" /> Apa itu program &#34;Tahfidhul Quran&#34; ?
                                <i className={`bx bx-chevron-down ${styles.icon_show}`}></i><i
                                className={`bx bx-chevron-up ${styles.icon_close}`}></i>
                            </a>
                            <div
                                id="faq-list-3"
                                className={`collapse ${activeAccordion === "faq-list-3" ? "show" : ""}`}
                                data-bs-parent=".faq-list"
                            >
                                <p>
                                    Program unggulan untuk santri yang berminat menghafal Alquran. Program ini bekerja
                                    sama
                                    dengan Jam&#39;iyatul Qurro&#39; wal Huffadz cabang Kota Kraksaan.
                                </p>
                            </div>
                        </li>

                        <li data-aos="fade-up" data-aos-delay="400">
                            <a
                                data-bs-toggle="collapse"
                                data-bs-target="#faq-list-4"
                                className={activeAccordion === "faq-list-4" ? "" : "collapsed"}
                                onClick={() => handleAccordionClick("faq-list-4")}
                                style={{ fontSize: '1.5rem' }}
                            >
                                <Icon icon="ph:seal-question-bold" color="#3fdede" width="32" height="32" /> Berapa Biaya SPP di MA Zainul Hasan 1 ?
                                <i className={`bx bx-chevron-down ${styles.icon_show}`}></i>
                                <i className={`bx bx-chevron-up ${styles.icon_close}`}></i>
                            </a>
                            <div
                                id="faq-list-4"
                                className={`collapse ${activeAccordion === "faq-list-4" ? "show" : ""}`}
                                data-bs-parent=".faq-list"
                            >
                                <p>
                                    Besaran biaya SPP berubah-ubah, besaran yang harus dibayarkan akan diberitahukan
                                    lebih
                                    lanjut pada periode tertentu.
                                </p>
                            </div>
                        </li>

                        <li data-aos="fade-up" data-aos-delay="500">
                            <a
                                data-bs-toggle="collapse"
                                data-bs-target="#faq-list-5"
                                className={activeAccordion === "faq-list-5" ? "" : "collapsed"}
                                onClick={() => handleAccordionClick("faq-list-5")}
                                style={{ fontSize: '1.5rem' }}
                            >
                                <Icon icon="ph:seal-question-bold" color="#3fdede" width="32" height="32" /> Apakah Harus Masuk Pondok ?
                                <i className={`bx bx-chevron-down ${styles.icon_show}`}></i>
                                <i className={`bx bx-chevron-up ${styles.icon_close}`}></i>
                            </a>
                            <div
                                id="faq-list-5"
                                className={`collapse ${activeAccordion === "faq-list-5" ? "show" : ""}`}
                                data-bs-parent=".faq-list"
                            >
                                <p>
                                    Ya, agar siswa di MA Zainul Hasan 1 dapat memperdalam ilmu Agama.
                                </p>
                            </div>
                        </li>

                    </ul>
                </div>

            </div>
        </section>
    );
}