import {Path, UseFormRegister} from "react-hook-form";
import React from "react";
import {UCFirst} from "mazaha/string";

interface IFormValues {
    nama: string,
    pertanyaan: string,
}

type InputProps = {
    label: Path<IFormValues>
    register: UseFormRegister<IFormValues>
    required: boolean,
    placeholder: string,
    defaultValue?: string
}
const Input = ({label, register, required, placeholder, defaultValue}: InputProps) => (
    <>
        <label className="form-label">{UCFirst(placeholder)}</label>
        <input {...register(label, {required})} className="form-control" placeholder={placeholder}
               defaultValue={defaultValue}/>
    </>
)

const TextArea = ({label, register, required, placeholder, defaultValue}: InputProps) => (
    <>
        <label className="form-label">{placeholder}</label>
        <textarea {...register(label, {required, maxLength: 200})} className="form-control" placeholder={placeholder}
                  defaultValue={defaultValue}/>
    </>
)

export {Input, TextArea};
export type {IFormValues};
