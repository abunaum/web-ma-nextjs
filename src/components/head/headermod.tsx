import Head from 'next/head';

interface dataprops {
    description: string;
    title: string;
    url: string;
}

export default function HeaderMod({data}: { data: dataprops }) {
    return (
        <>
            <Head>
                <meta charSet="utf-8"/>
                <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
                <title>MA ZAHA 1 - {(data.title)}</title>
                <link rel="canonical" href={(data.url)}/>
                <link rel="next" href={(data.url)}/>
                <link rel="icon" href="/LOGO.png"/>
                <meta name="description" content={(data.description)}/>
                <meta property="og:description" content={(data.description)}/>
                <meta property="og:locale" content="id_ID"/>
                <meta property="og:type" content="website"/>
                <meta property="og:title" content={(data.title)}/>
                <meta property="og:url" content={(data.url)}/>
                <meta property="og:site_name" content="MA ZAHA 1 Genggong"/>
                <meta name="twitter:card" content="summary_large_image"/>
                <meta name="theme-color" content="#6777ef"/>
                <link rel="apple-touch-icon"
                      href="https://res.cloudinary.com/dfko3mdsp/image/upload/v1690253988/website/qvke2obb8ycpwylk8a9h.png"/>
                <link
                    href="https://res.cloudinary.com/dfko3mdsp/image/upload/v1690253988/website/qvke2obb8ycpwylk8a9h.png"
                    rel="icon"/>
                <link
                    href="https://res.cloudinary.com/dfko3mdsp/image/upload/v1690253988/website/qvke2obb8ycpwylk8a9h.png"
                    rel="apple-touch-icon"/>
            </Head>
        </>
    )
}