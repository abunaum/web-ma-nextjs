import Head from 'next/head';

export default function HeaderDefault() {
    return (
        <>
            <Head>
                <link rel="manifest" href="/manifest.json"/>
                <link rel="canonical" href="https://mazainulhasan1.sch.id/"/>
                <link rel="next" href="https://mazainulhasan1.sch.id"/>
                <link rel="icon" href="/LOGO.png"/>
                <link rel="apple-touch-icon"
                      href="https://res.cloudinary.com/dfko3mdsp/image/upload/v1690253988/website/qvke2obb8ycpwylk8a9h.png"/>
                <link
                    href="https://res.cloudinary.com/dfko3mdsp/image/upload/v1690253988/website/qvke2obb8ycpwylk8a9h.png"
                    rel="icon"/>
                <link
                    href="https://res.cloudinary.com/dfko3mdsp/image/upload/v1690253988/website/qvke2obb8ycpwylk8a9h.png"
                    rel="apple-touch-icon"/>
                {/*<meta name="description" content="Website Resmi MA Zainul Hasan 1 Genggong"/>*/}
                <meta name="keywords"
                      content="ma, zaha, sekolah, swasta, zainul, hasan, madrasah, probolinggo, genggong, jawa, timur, aliyah, indonesia, pendidikan, atas"/>
                <meta property="og:type" content="website"/>
                <meta property="og:url" content="https://mazainulhasan1.sch.id/"/>
                {/*<meta property="og:description" content="Website Resmi MA Zainul Hasan 1 Genggong"/>*/}
                <meta property="og:site_name" content="MA Zainul Hasan 1 Genggong"/>
                {/*<meta property="og:image"*/}
                {/*      content="https://res.cloudinary.com/dfko3mdsp/image/upload/v1690253988/website/qvke2obb8ycpwylk8a9h.png"/>*/}
                <meta name="twitter:card" content="summary_large_image"/>
                {/*<meta name="twitter:title" content="MA Zainul Hasan 1 Genggong"/>*/}
                <meta name="theme-color" content="#6777ef"/>
            </Head>
        </>
    )
}