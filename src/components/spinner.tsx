import React from "react";
import styles from "@/styles/spinner.module.css";

type SpinnerProps = {
    text: string,
    extended?: string,
};

const Spinner: React.FC<SpinnerProps> = ({ text, extended }) => {
    return (
        <React.Fragment>
            <center>
                <p className="text-center" style={{color: "black"}}>{text}</p>
                {extended && <span dangerouslySetInnerHTML={{ __html: extended }} />}
                <br/>
                <div className={styles.spinner}>
                    <div className={styles.lds_roller}>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                    <div></div>
                    <div></div>
                </div>
                <div>&nbsp;</div>
            </center>
        </React.Fragment>
    );
};

export default Spinner;