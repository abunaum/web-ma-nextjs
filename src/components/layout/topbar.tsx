"use client";
import React from "react";
import Image from "next/image";
import Link from "next/link";
import header from "@/styles/layout/header.module.css"
import navbar from "@/styles/layout/navbar.module.css"

export default function TopBar() {
    const [isNavOpen, setIsNavOpen] = React.useState<boolean>(false);
    const [Dtk, setDtk] = React.useState<boolean>(false);

    const toggleNav = () => {
        setIsNavOpen(!isNavOpen);
    };

    const CloseMobile = () => {
        setIsNavOpen(false);
        setDtk(false);
    };

    const dtk = () => {
        setDtk(!Dtk);
    };
    return (
        <header className={`fixed-top ${header.header}`}>
            <div className="container d-flex align-items-center">
                <h1 className={`${header.logo} me-auto`}>
                    <Link
                        href={{
                            pathname: '/',
                        }}
                    >
                        <Image
                            src="/ma-white.png"
                            alt="MA Logo"
                            className="img-fluid"
                            width={100}
                            height={24}
                            priority
                            style={{height: 'auto', width: 'auto'}}
                        />
                    </Link>
                </h1>

                <nav className={`${isNavOpen ? navbar.navbar_mobile : navbar.navbar}`}>
                    <ul>
                        <li>
                            <Link
                                href={{
                                    pathname: '/',
                                }}
                                className={`${navbar.navlink} scrollto`}
                                onClick={CloseMobile}
                            >
                                Home
                            </Link>
                        </li>
                        <li className={navbar.dropdown}>
                            <a href="#" className={`${navbar.navlink} scrollto`} onClick={dtk}>
                                <span>Tentang Kami</span> <i className="bi bi-chevron-down"></i>
                            </a>
                            <ul className={`${Dtk ? navbar.dropdown_active : ''}`}>
                                <li>
                                    <Link
                                        className={`${navbar.navlink} scrollto`}
                                        href={{
                                            pathname: '/sambutan',
                                        }}
                                        onClick={CloseMobile}
                                    >
                                        <span>Sambutan Kepala Madrasah</span>
                                    </Link>
                                </li>
                                <li>
                                    <Link
                                        className={`${navbar.navlink} scrollto`}
                                        href={{
                                            pathname: '/profile_madrasah',
                                        }}
                                        onClick={CloseMobile}
                                    >
                                        Profile Madrasah
                                    </Link>
                                </li>
                                <li>
                                    <Link
                                        className={`${navbar.navlink} scrollto`}
                                        href={{
                                            pathname: '/visi_misi',
                                        }}
                                        onClick={CloseMobile}
                                    >
                                        Visi-Misi
                                    </Link>
                                </li>
                                <li>
                                    <Link
                                        className={`${navbar.navlink} scrollto`}
                                        href={{
                                            pathname: '/struktur_organisasi',
                                        }}
                                        onClick={CloseMobile}
                                    >
                                        Struktur Organisasi
                                    </Link>
                                </li>
                                <li>
                                    <Link
                                        className={`${navbar.navlink} scrollto`}
                                        href={{
                                            pathname: '/tenaga_madrasah',
                                        }}
                                        onClick={CloseMobile}
                                    >
                                        Tenaga Madrasah
                                    </Link>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <Link
                                href={{
                                    pathname: '/program',
                                }}
                                className={`${navbar.navlink} scrollto`}
                                onClick={CloseMobile}
                            >
                                Program
                            </Link>
                        </li>
                        <li>
                            <Link
                                href={{
                                    pathname: '/informasi',
                                }}
                                className={`${navbar.navlink} scrollto`}
                                onClick={CloseMobile}
                            >
                                Informasi
                            </Link>
                        </li>
                        <li>
                            <Link
                                href={{
                                    pathname: '/kontak',
                                }}
                                className={`${navbar.navlink} scrollto`}
                                onClick={CloseMobile}
                            >
                                Kontak
                            </Link>
                        </li>
                        <li>
                            <Link
                                href={{
                                    pathname: '/download_file',
                                }}
                                className={`${navbar.navlink} scrollto`}
                                onClick={CloseMobile}
                            >
                                Download
                            </Link>
                        </li>
                        <li><a className={`${navbar.getstarted} ${navbar.navlink} scrollto`}
                               href="https://ppsb.mazainulhasan1.sch.id"
                               target="_blank">PPSB</a></li>
                    </ul>
                    <i className={`bi ${isNavOpen ? navbar.mobile_nav_toggle + ' bi-x' : navbar.mobile_nav_toggle + ' bi-list'}`}
                       onClick={toggleNav}></i>
                </nav>
            </div>
        </header>
    )
}