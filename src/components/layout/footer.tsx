import Image from "next/image";
import {Icon} from '@iconify/react';
import Link from "next/link";
import React from "react";
import styles from "@/styles/layout/footer.module.css";

export default function FOOTER() {
    return (
        <>
            <div className={styles.footer_top} style={{backgroundColor: "#f0fcfb"}}>
                <div className="container">
                    <div className="row">
                        <div className="col-lg-3 col-md-6 footer-contact">
                            <Image
                                src="/assets/img/ma-black.png"
                                alt="Logo Animasi"
                                className="img-fluid"
                                width={500}
                                height={300}
                                priority
                                style={{width: '15rem'}}
                            />
                            <div className={styles.ctc}>
                                Jl. Raya Condong No.12, Gerojokan<br/>
                                Karangbong, Kec. Pajarakan<br/>
                                Kab. Probolinggo, Jawa Timur, 67281<br/>
                                <strong>Telepon: </strong>(0335) 842253<br/>
                                <strong>Email: </strong>mazainulhasan1@gmail.com<br/>
                            </div>
                        </div>

                        <div className={`col-lg-3 col-md-6 ${styles.footer_links}`}>
                            <h4>Link</h4>
                            <ul>
                                <li>
                                    <i className="bx bx-chevron-right"></i>
                                    <Link
                                        href={{
                                            pathname: '/profile_madrasah',
                                        }}
                                    >
                                        Profile
                                    </Link>
                                </li>
                                <li>
                                    <i className="bx bx-chevron-right"></i>
                                    <Link
                                        href={{
                                            pathname: '/visi_misi',
                                        }}
                                    >
                                        Visi & Misi
                                    </Link>
                                </li>
                                <li>
                                    <i className="bx bx-chevron-right"></i>
                                    <Link
                                        href={{
                                            pathname: '/tenaga_madrasah',
                                        }}
                                    >
                                        Tenaga Madrasah
                                    </Link>
                                </li>
                            </ul>
                        </div>

                        <div className={`col-lg-3 col-md-6 ${styles.footer_links}`}>
                            <h4>Layanan</h4>
                            <ul>
                                <li><i className="bx bx-chevron-right"></i> <a href="https://ppsb.mazainulhasan1.sch.id"
                                                                               target="_blank">PSB</a></li>
                                <li><i className="bx bx-chevron-right"></i> <a
                                    href="https://simumtaz.mazainulhasan1.sch.id" target="_blank">SIMUMTAZ</a></li>
                                <li>
                                    <i className="bx bx-chevron-right"></i>
                                    <Link
                                        href={{
                                            pathname: '/berita',
                                        }}
                                    >
                                        Berita
                                    </Link>
                                </li>
                                <li>
                                    <i className="bx bx-chevron-right"></i>
                                    <Link
                                        href={{
                                            pathname: '/download_file',
                                        }}
                                    >
                                        Download
                                    </Link>
                                </li>
                            </ul>
                        </div>

                        <div className={`col-lg-3 col-md-6 ${styles.footer_links}`}>
                            <h4>Sosial Media Kami</h4>
                            <p>Hubungi kami via sosial media</p>
                            <div className="mt-3">
                                <a href="https://t.me/ma_zahagenggong" className="p-1">
                                    <Icon icon="logos:telegram" width="32" height="32"/>
                                </a>
                                <a href="https://www.facebook.com/mazahagenggong" className="p-1">
                                    <Icon icon="logos:facebook" width="32" height="32"/>
                                </a>
                                <a href="https://www.instagram.com/ma_zahagenggong" className="p-1">
                                    <Icon icon="skill-icons:instagram" width="32" height="32"/>
                                </a>
                                <a href="https://www.youtube.com/@mazaha1genggong879" className="p-1">
                                    <Icon icon="fa:youtube-square" width="32" height="32" color="red"/>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div className={`${styles.footer_bottom} clearfix`}>
                <div className="container d-md-row py-2">
                    <div className={styles.copyright}>
                        &copy; Copyright <strong><span>MA ZAHA 1</span></strong>. All Rights Reserved
                    </div>
                    <div className={styles.credits}>
                        Developed by TIM IT MA Zainul Hasan 1 Genggong
                    </div>
                </div>
            </div>
        </>
    );
}