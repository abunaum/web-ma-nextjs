const MaintenancePage = () => {
    return (
        <div
            style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
                height: '100vh',
                textAlign: 'center',
                backgroundColor: '#f4f4f4',
                color: '#333',
                fontFamily: 'Arial, sans-serif',
                padding: '20px',
            }}
        >
            <h1
                style={{
                    fontSize: '2.5rem',
                    fontWeight: 'bold',
                    marginBottom: '20px',
                    color: '#06b317',
                }}
            >
                Situs Sedang Dalam Pemeliharaan
            </h1>
            <p
                style={{
                    fontSize: '1.25rem',
                    marginBottom: '20px',
                    maxWidth: '600px',
                    lineHeight: '1.6',
                }}
            >
                Kami sedang melakukan pembaruan untuk meningkatkan layanan.
                Harap kembali lagi nanti. Terima kasih atas pengertiannya!
            </p>
            <div
                style={{
                    width: '100px',
                    height: '4px',
                    backgroundColor: '#06b317',
                    borderRadius: '2px',
                    marginBottom: '20px',
                }}
            ></div>
            <p
                style={{
                    fontSize: '1rem',
                    color: '#666',
                }}
            >
                Jika Anda memiliki pertanyaan mendesak, silakan hubungi kami di{' '}
                <a
                    href="mailto:mazainulhasan1@gmail.com"
                    style={{ color: '#06b317', textDecoration: 'none' }}
                >
                    mazainulhasan1@gmail.com
                </a>
            </p>
        </div>
    );
};

export default MaintenancePage;
